# SIMTIO - Rocketchat

This container contain Rocketchat.

## Minimal Startup

### Running Mongodb instance
```
docker run --name simtio_mongodb -d -p 27017:27017 -v mongodb-initdb:/docker-entrypoint-initdb.d/ \
registry.gitlab.com/simtio/briques/simtio_mongodb:latest \
--smallfiles --replSet rs01 --oplogSize 128
```

You need to put files/initiate-mongodb.sh in mongodb-initdb volume.

### Running rocketchat instance
```
docker run --name simtio_rocketchat -d -p 3000:3000 \
-e MONGO_URL=mongodb://simtio_mongodb:27017/rocketchat \
-e MONGO_OPLOG_URL=mongodb://simtio_mongodb:27017/local \
simtio/rocketchat:latest
```

## Dev
```
docker build -t simtio/rocketchat:latest -t simtio/rocketchat:0.x .
docker push simtio/rocketchat:latest && docker push simtio/rocketchat:0.x

docker run --rm --name simtio_rocketchat -p 3000:3000 --link simtio_mongodb \
-e MONGO_URL=mongodb://simtio_mongodb:27017/rocketchat \
-e MONGO_OPLOG_URL=mongodb://simtio_mongodb:27017/local \
simtio/rocketchat:latest
```

Multiarch :
* `docker buildx build -t simtio/rocketchat:latest -t simtio/rocketchat:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push`

## Sources
- https://rocket.chat/docs/installation/manual-installation/debian/
- https://hub.docker.com/_/rocket-chat
